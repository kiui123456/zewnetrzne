﻿namespace kosiazkaadresowa.ViewModels
{
    using System;
    using Models;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Documents;
    using System.Collections.Generic;
    using System.Windows.Data;
    //Dwie podstawowe deklaracjie 
    //Biblioteka MVVM Light
    using GalaSoft.MvvmLight.Command;
    //wbudowana biblioteka objektowaości dla MVVM
    using System.Collections.ObjectModel;
    using System.Xml;
    using System.Xml.Linq;
    using System.IO;
    using GalaSoft.MvvmLight;
    using System.Xml.Serialization;
    //typ dostępu

    public class KsiazkaAdresowaViewModel : ViewModelBase
    {
        #region Consts
        //stała nazwy pliku
        const string KSIAZKA_FILENAME = "KS.xml";

        #endregion

        #region Kontruktor

        public KsiazkaAdresowaViewModel()
        {
            ZaladujKsiazkeAdresowa();

        }

        #endregion

        #region Wlasciwosci implementujace IPropertyChanged

        #region Adresat

        public const string AdresatPropertyName = "Adresat";
        private AdresatViewModel adresat;
        public AdresatViewModel Adresat
        {
            get
            {
                if (this.adresat == null)
                {
                    this.adresat = new AdresatViewModel();
                }

                return this.adresat;
            }

            set
            {
                if (this.adresat == value) return;
                this.adresat = value;
                RaisePropertyChanged(AdresatPropertyName);
            }
        }

        #endregion

        #region Adresaci

        public const string AdresaciPropertyName = "Adresaci";
        private ObservableCollection<AdresatViewModel> adresaci;
        public ObservableCollection<AdresatViewModel> Adresaci
        {
            get
            {
                return this.adresaci;
            }

            set
            {
                if (this.adresaci == value) return;
                this.adresaci = value;
                RaisePropertyChanged(AdresaciPropertyName);
            }
        }

        #endregion

        #region WybranyAdresat

        public const string WybranyAdresatPropertyName = "WybranyAdresat";
        private AdresatViewModel wybranyAdresat;
        public AdresatViewModel WybranyAdresat
        {
            get
            {
                return this.wybranyAdresat;
            }

            set
            {
                if (this.wybranyAdresat == value) return;
                this.wybranyAdresat = value;
                RaisePropertyChanged(WybranyAdresatPropertyName);
            }
        }

        #endregion

        #endregion

        #region Komendy

        #region DodajAdresataCommand

        private ICommand dodajAdresataCommand;

        public ICommand DodajAdresataCommand
        {
            get
            {
                if (this.dodajAdresataCommand == null)
                {
                    this.dodajAdresataCommand = new RelayCommand(() =>
                    {
                        var adresat = Adresat;
                        Adresat = new AdresatViewModel();
                        Adresaci.Add(adresat);
                        WybranyAdresat = adresat;
                    });
                }

                return this.dodajAdresataCommand;
            }
        }

        #endregion

        #region UsunAdresataCommand

        private ICommand usunAdresataCommand;

        public ICommand UsunAdresataCommand
        {
            get
            {
                if (this.usunAdresataCommand == null)
                {
                    this.usunAdresataCommand = new RelayCommand(() =>
                    {
                        var adresat = WybranyAdresat;
                        WybranyAdresat = null;
                        Adresaci.Remove(adresat);

                        if (Adresaci.Count > 0)
                        {
                            WybranyAdresat = Adresaci[0];
                        }
                    });
                }

                return this.usunAdresataCommand;
            }
        }

        #endregion

        #region OdczytajKsiazkeCommand

        private ICommand odczytajKsiazkeCommand;

        public ICommand OdczytajKsiazkeCommand
        {
            get
            {
                if (this.odczytajKsiazkeCommand == null)
                {
                    this.odczytajKsiazkeCommand = new RelayCommand(() =>
                    {
                        ZaladujKsiazkeAdresowa();
                    });
                }

                return this.odczytajKsiazkeCommand;
            }
        }

        #endregion

        #region ZapiszKsiazkeCommand

        private ICommand zapiszKsiazkeCommand;

        public ICommand ZapiszKsiazkeCommand
        {
            get
            {
                if (this.zapiszKsiazkeCommand == null)
                {
                    this.zapiszKsiazkeCommand = new RelayCommand(() =>
                    {
                        ZapiszKsiazkeAdresowa();
                    });
                }

                return this.zapiszKsiazkeCommand;
            }
        }

        #endregion

        #endregion

        #region Methods

        public void ZaladujKsiazkeAdresowa()
        {
            List<Adresat> listAdresatow = odczyt().Adresaci;
            
            Adresaci = new ObservableCollection<AdresatViewModel>();

            foreach (var adresat in listAdresatow)
            {
                Adresaci.Add(new AdresatViewModel
                {
                    Nazwisko = adresat.Nazwisko
                });
            }
        }

        public void ZapiszKsiazkeAdresowa()
        {
            var ksiazkaAdresowa = new KsiazkaAdresowa();

            foreach (var adresatVM in Adresaci)
            {
                ksiazkaAdresowa.Adresaci.Add(new Adresat
                {
                    Nazwisko = adresatVM.Nazwisko
                });
            }

            zapiszksiazke(ksiazkaAdresowa);
        }

        public KsiazkaAdresowa odczyt()
        {
            KsiazkaAdresowa ksiazkaAdresowa = null;

            try
            {
                using (var fs = new FileStream(KSIAZKA_FILENAME, FileMode.Open, FileAccess.Read))
                {
                    var serializer = new XmlSerializer(typeof(KsiazkaAdresowa));
                    ksiazkaAdresowa = (KsiazkaAdresowa)serializer.Deserialize(fs);
                }
            }
            catch (Exception ex)
            {
                ksiazkaAdresowa = new KsiazkaAdresowa();
            }

            return ksiazkaAdresowa;
        }

        public void zapiszksiazke(KsiazkaAdresowa ksiazkaAdresowa)
        {
            using (var fs = new FileStream(KSIAZKA_FILENAME, FileMode.OpenOrCreate, FileAccess.Write))
            {
                var serializer = new XmlSerializer(typeof(KsiazkaAdresowa));
                serializer.Serialize(fs, ksiazkaAdresowa);
            }
        }

        #endregion
    }
}
