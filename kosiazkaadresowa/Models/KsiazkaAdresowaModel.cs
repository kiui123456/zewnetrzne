﻿

namespace kosiazkaadresowa.Models
{
    using GalaSoft.MvvmLight;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Documents;
    class KsiazkaAdresowaModel : ViewModelBase
    {
        public KsiazkaAdresowaModel(string nazwaadresata, int iloscadrestow)
        {
            NazwaAdresata = nazwaadresata;
            Iloscadrestow = iloscadrestow;
        }
        #region Adresat
        private string _NazwaAdresata;
        public string NazwaAdresata
        {
            get
            {
                return _NazwaAdresata;
            }
            set
            {
                _NazwaAdresata = value;
                RaisePropertyChanged("NazwaAdresata");

            }
        }
        #endregion

        #region ilosc adrestaow
        private int _Iloscadrestaow;
        public int Iloscadrestow
        {
            get
            {
                return _Iloscadrestaow;
            }
            set
            {
                if (this._Iloscadrestaow == value) return;
                this._Iloscadrestaow = value;
                RaisePropertyChanged("Iloscadrestow");
            }
        }




        #endregion

        //#region INOTYFITY Member
        //public event PropertyChangedEventHandler PropertyChanged;
        //private void OnProperytyChenged(string p)
        //{
        //    PropertyChangedEventHandler handler = PropertyChanged;


        //    if (handler != null)
        //    {
        //        handler(this, new PropertyChangedEventArgs(p));
        //    }
        //}



        //#endregion








    }
}
