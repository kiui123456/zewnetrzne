﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight;
namespace kosiazkaadresowa.Models 
{
    class KSAdresaciModels : ViewModelBase
    {

        //konstruktor klasy KSAdresaciModels
        public KSAdresaciModels(ObservableCollection<string> adresaci)
        {

            _Adresaci = adresaci;
        }
        public int AdresatDoUsuniecia { get; set; }
        

        #region wyswietl pobierz adresatow
        public ObservableCollection<string> _Adresaci;
        public ObservableCollection<string> Adresaci
        {
            get
            {
                return _Adresaci;
            }
            set
            {
                if (this._Adresaci == value) return;
                this._Adresaci = value;
                RaisePropertyChanged("Adresaci");
            }
        }

        #endregion
        
        /// <summary>
        /// Metoda pozwalajaca załadować dane do lisboxu
        /// </summary>
        //public event PropertyChangedEventHandler PropertyChanged;
        //private void RaisePropertyChanged(string propertyName)
        //{
        //    if (this.PropertyChanged != null)
        //        this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        //}
    }
}
